package info.hccis.camper.data.springdatajpa;

import info.hccis.camper.jpa.UserAccess;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAccessRepository extends CrudRepository<UserAccess, Integer> {

    List<UserAccess> findByUsername(String username);


}
