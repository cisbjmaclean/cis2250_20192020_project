package info.hccis.camper.entity;

import java.io.Serializable;

/**
 * This class will represent a camper.
 *
 * @author bjmaclean
 * @since 20150915
 */
public class Camper extends info.hccis.camper.jpa.Camper implements Serializable {

    public static final int NUMBER_CAMPERS = 50;
    public static final int RECORD_SIZE = 150;

//

    public Camper(){
        
    }
    

    /**
     * Custom constructor with all info
     *
     * @param id
     * @param firstName
     * @param lastName
     * @param dob
     *
     * @author BJ MacLean
     * @since 20150917
     */
    public Camper(int id, String firstName, String lastName, String dob) {
        super(id,firstName, lastName);
        setDob(dob);
    }


}
