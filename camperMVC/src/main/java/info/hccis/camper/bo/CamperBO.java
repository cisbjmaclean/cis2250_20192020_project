package info.hccis.camper.bo;

import info.hccis.camper.data.springdatajpa.CamperRepository;
import info.hccis.camper.jpa.Camper;

/**
 * Contains camper model functionality.
 * @author bjmaclean
 * @since Nov 6, 2019
 */
public class CamperBO {

    CamperRepository cr = null;
    
    public CamperBO(CamperRepository cr) {
        this.cr = cr;
    }

    public Camper getCamper(int id){
        return cr.findOne(id);
    }


}
