package info.hccis.util;

import com.google.gson.Gson;
import info.hccis.camper.jpa.Camper;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author bjmaclean
 * @since Nov 17, 2017
 */
public class UtilityRest {

    public static String getWeatherNote(){
                    //check the weather
            final String urlForWeather = "http://apidev.accuweather.com/currentconditions/v1/1327.json?language=en&apikey=hoArfRosT1215";
            String jsonReturned = UtilityRest.getJsonFromRest(urlForWeather);
            JSONArray jsonArray = new JSONArray(jsonReturned);
            JSONObject jsonObject1 = jsonArray.getJSONObject(0);
            JSONObject temperature = jsonObject1.getJSONObject("Temperature");
            JSONObject metric = temperature.getJSONObject("Metric");
            float currentTemperature = metric.getFloat("Value");
            System.out.println("BJM temperature="+currentTemperature);
            String extraMessage = "No beach today";
            if (currentTemperature > 18) {
                extraMessage = "Bring your bathing suite to camp";
            }
            return extraMessage;

    }
    
    /**
     * This method will call the rest web service and give back the json
     *
     * @since 20171117
     * @author BJM
     */
    public static String getJsonFromRest(String urlString) {

        String content = "";
        try {

            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() == 204) {
                System.out.println("No data found");
            } else if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;

            while ((output = br.readLine()) != null) {
                content += output;
            }

            conn.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }

}
