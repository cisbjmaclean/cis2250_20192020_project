package info.hccis.admin.util;

import info.hccis.admin.dao.CodeValueDAO;
import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.jpa.CodeType;
import info.hccis.admin.model.jpa.CodeValue;
import info.hccis.admin.service.CodeService;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;

/**
 * This class will be used to hold generic code related methods.
 *
 * @author bjmaclean
 * @since 20170503
 */
public class UtilCodes {

    public static void updateSessionCodes(HttpServletRequest request, CodeService codeService) {
        DatabaseConnection databaseConnection = (DatabaseConnection) request.getSession().getAttribute("db");

        ArrayList<CodeType> codes = codeService.getCodeTypes(databaseConnection);
//        model.addAttribute("codeTypes", codes);
        ArrayList<CodeValue> UserTypes = (ArrayList<CodeValue>) CodeValueDAO.getCodeValues(databaseConnection, "1");
        request.getSession().setAttribute("UserTypes", UserTypes);
        ArrayList<CodeValue> golfCourseTypes = (ArrayList<CodeValue>) CodeValueDAO.getCodeValues(databaseConnection, "2");
        request.getSession().setAttribute("golfCourseTypes", golfCourseTypes);
        ArrayList<CodeValue> chicagoPointTypes = (ArrayList<CodeValue>) CodeValueDAO.getCodeValues(databaseConnection, "3");
        request.getSession().setAttribute("chicagoPointTypes", chicagoPointTypes);

        int[] points = new int[chicagoPointTypes.size()];
        for (int i = 0; i < chicagoPointTypes.size(); i++) {
            points[i] = Integer.parseInt(chicagoPointTypes.get(i).getEnglishDescription());
        }
        System.out.println("chicagoPoints=" + points);
        request.getSession().setAttribute("chicagoPoints", points);
    }

}
