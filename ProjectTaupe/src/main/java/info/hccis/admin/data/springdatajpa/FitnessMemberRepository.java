package info.hccis.admin.data.springdatajpa;

import info.hccis.admin.model.entity.Fitnessmember;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FitnessMemberRepository extends CrudRepository<Fitnessmember, Integer> {


}