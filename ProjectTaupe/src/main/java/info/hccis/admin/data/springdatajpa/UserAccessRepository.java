package info.hccis.admin.data.springdatajpa;

import info.hccis.admin.model.entity.Useraccess;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAccessRepository extends CrudRepository<Useraccess, Integer> {


}