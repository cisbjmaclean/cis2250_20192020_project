package info.hccis.admin.data.springdatajpa;

import info.hccis.admin.model.entity.CourtInformation;
import info.hccis.admin.model.entity.Courtbooking;
import info.hccis.admin.util.Utility;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookingRepository extends CrudRepository<Courtbooking, Integer> {
   

}
