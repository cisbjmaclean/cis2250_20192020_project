package info.hccis.admin.data.springdatajpa;

import info.hccis.admin.model.jpa.CodeType;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CodeTypeRepository extends CrudRepository<CodeType, Integer> {

    List<CodeType> findByEnglishDescription(String englishDescription);
}