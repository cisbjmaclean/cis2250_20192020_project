/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.dao;

import info.hccis.admin.dao.util.ConnectionUtils;
import info.hccis.admin.dao.util.DbUtils;
import info.hccis.admin.model.entity.Courtbooking;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 *
 * @author mblanchard9255
 */
public class CourtBookingDAO {
    
    private final static Logger LOGGER = Logger.getLogger(CourtBookingDAO.class.getName());
    
    public static ArrayList<Courtbooking> searchForBookingByDateRange(int searchId, String startDate, String endDate) {

        ArrayList<Courtbooking> courtBookings = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM CourtBooking WHERE (memberId=" + searchId + " OR memberIdOpponent=" + searchId
                    + ") AND bookingDate>=" + startDate + " AND bookingDate<=" + endDate;

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                int id = rs.getInt("id");
                int courtNumber = rs.getInt("courtNumber");
                String bookingDate = rs.getString("bookingDate");
                String startTime = rs.getString("startTime");
                int memberId = rs.getInt("memberId");
                int memberIdOpponent = rs.getInt("memberIdOpponent");
                String notes = rs.getString("notes");
                String createdDate = rs.getString("createdDate");
             
                courtBookings.add(new Courtbooking(id, courtNumber, bookingDate, startTime, memberId, memberIdOpponent, notes, createdDate));
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return courtBookings;
    }
    
}
