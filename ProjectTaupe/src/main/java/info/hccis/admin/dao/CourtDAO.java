package info.hccis.admin.dao;

import info.hccis.admin.dao.util.ConnectionUtils;
import info.hccis.admin.dao.util.DbUtils;
import info.hccis.admin.model.entity.CourtInformation;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Connect to the database, court table
 *
 * @author bjmaclean modified by Brandon Campbell
 * @since 20160929 modified Dec 11 2019
 */
public class CourtDAO {

    private final static Logger LOGGER = Logger.getLogger(CourtDAO.class.getName());

    /**
     * Select one court and their information
     *
     * @author bjmaclean modified by Brandon Campbell
     * @since 20160929 modified Dec 11 2019
     * @return
     */
    public static CourtInformation select(int idIn) {
        //This method gets passed an id, then looks in the court table for that id and gets the information
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        CourtInformation theCourt = null;
        try {
            conn = ConnectionUtils.getConnection();
            System.out.println("Loading for: " + idIn);
            sql = "SELECT * FROM Court where id = ?";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, idIn);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                //get selected court information
                int id = rs.getInt("id");
                int courtNumber = rs.getInt("courtNumber");
                String courtName = rs.getString("courtName");
                int courtType = rs.getInt("courtType");

                theCourt = new CourtInformation(id, courtNumber, courtName, courtType);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        //returns found court
        return theCourt;
    }
    /**
     * Select all courts and their information
     *
     * @author bjmaclean modified by Brandon Campbell
     * @since 20160929 modified Dec 11 2019
     * @return
     */
    public static ArrayList<CourtInformation> selectAll() {
        //Select all the courts that are in the database
        ArrayList<CourtInformation> courts = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM Court ORDER BY id";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                //gets the court information
                int id = rs.getInt("id");
                int courtNumber = rs.getInt("courtNumber");
                String courtName = rs.getString("courtName");
                int courtType = rs.getInt("courtType");

                courts.add(new CourtInformation(id, courtNumber, courtName, courtType));
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        //returns all the courts
        return courts;
    }

    /**
     * This method will delete a court based on the
     * id passed in.
     *
     * @author BJ modified Brandon Campbell
     * @since 20140615 modified Oct 25 2019
     */
    public static synchronized void delete(int id) throws Exception {
        //deletes a court based on passed in id number
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;


        /*
         * Setup the sql to update or insert the row.
         */
        try {
            conn = ConnectionUtils.getConnection();

            sql = "DELETE FROM Court WHERE id=?";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);

            /*
             Note executeUpdate() for update vs executeQuery for read only!!
             */
            ps.executeUpdate();

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw e;
        } finally {
            DbUtils.close(ps, conn);
        }

    }

    /**
     * This method will update the courts information
     *
     * @return
     * @author BJ modified Oct 25 2019
     * @since 20140615 modified Oct 25 2019
     */
    public static synchronized CourtInformation update(CourtInformation court) throws Exception {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        /*
         * Setup the sql to update or insert the row.
         */
        try {
            conn = ConnectionUtils.getConnection();

            //Check to see if court exists.
            //Note that the default for an Integer is null not 0  !!!
            //When I try to compare the getId() to null it throws an exception.
            int courtIdInt;
            try {
                courtIdInt = court.getCourtId();
            } catch (Exception e) {
                courtIdInt = 0;

            }
            
            if (courtIdInt == 0) {

                sql = "SELECT max(id) from Court";
                ps = conn.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();
                int max = 0;
                while (rs.next()) {
                    max = rs.getInt(1) + 1;
                }

                court.setCourtId(max);

                sql = "INSERT INTO `Court`(`id`, `courtNumber`, `courtName`, `courtType`) "
                        + "VALUES (?,?,?,?)";

                ps = conn.prepareStatement(sql);
                ps.setInt(1, court.getCourtId());
                ps.setInt(2, court.getCourtNumber());
                ps.setString(3, court.getCourtName());
                ps.setInt(4, court.getCourtType());

            } else {

                sql = "UPDATE `Court` SET `courtNumber`=?,`courtName`=?,`courtType`=? WHERE id = ?";

                ps = conn.prepareStatement(sql);
                ps.setInt(1, court.getCourtNumber());
                ps.setString(2, court.getCourtName());
                ps.setInt(3, court.getCourtType());
                ps.setInt(4, court.getCourtId());

            }
            /*
             Note executeUpdate() for update vs executeQuery for read only!!
             */

            ps.executeUpdate();

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw e;
        } finally {
            DbUtils.close(ps, conn);
        }
        return court;

    }

}
