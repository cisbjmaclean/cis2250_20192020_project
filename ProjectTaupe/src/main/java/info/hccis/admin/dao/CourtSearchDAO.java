package info.hccis.admin.dao;

import info.hccis.admin.dao.util.ConnectionUtils;
import info.hccis.admin.dao.util.DbUtils;
import info.hccis.admin.model.entity.CourtInformation;
import info.hccis.admin.model.entity.CourtSearch;
import info.hccis.admin.model.entity.CourtSearchFound;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Logger;

/**
 * search the courts table for the report
 *
 * @author Brandon
 * @since Dec 11 2019
 */
public class CourtSearchDAO {

    private final static Logger LOGGER = Logger.getLogger(CourtSearchDAO.class.getName());

    /**
     * Select all courts that match search criteria
     *
     * @author Brandon
     * @since Dec 11 2019
     */
    public static ArrayList<CourtSearchFound> selectAll(CourtSearch search) throws ParseException {
        //Searching for all courts that can be booked for the entered book date
        ArrayList<CourtSearchFound> courts = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        try {
            conn = ConnectionUtils.getConnection();
            sql = "SELECT bookingDate \n"
                    + "FROM CourtBooking\n"
                    + "WHERE bookingDate=? \n";
            ps = conn.prepareStatement(sql);
            ps.setString(1, search.getBookingDate());
            ResultSet rs1 = ps.executeQuery();
            String bookingDateFound = null;
            while (rs1.next()) {
                bookingDateFound = rs1.getString("bookingDate");
            }
            System.out.println(bookingDateFound);
            //if there isn't a matching bookingDate in the database, first query is run
            if (bookingDateFound == null) {
                sql = "SELECT c.courtNumber,c.courtName,c.courtType,t.startTime \n"
                        + "FROM  Court as c,CourtTimes as t\n"
                        + "WHERE c.courtType=? and t.startTime>=?\n"
                        + "order by c.courtNumber,t.startTime";
                ps = conn.prepareStatement(sql);
                ps.setInt(1, search.getCourtType());
                ps.setString(2, search.getStartTime());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    int courtNumber = rs.getInt("courtNumber");
                    String courtName = rs.getString("courtName");
                    int courtType = rs.getInt("courtType");
                    String startTime = rs.getString("startTime");
                    System.out.println("courtInfoSaved");
                    courts.add(new CourtSearchFound(courtNumber, courtName, courtType, search.getBookingDate(), startTime));
                }
            } else {
                //else if bookingDate is in the database, this sql is run
                sql = "SELECT c.courtNumber,c.courtName,c.courtType,b.bookingDate,t.startTime \n"
                        + "FROM CourtBooking AS b, Court as c,CourtTimes as t\n"
                        + "WHERE b.bookingDate=? and c.courtType=? and t.startTime!=b.startTime and t.startTime>=?\n"
                        + "order by c.courtNumber,t.startTime";
                ps = conn.prepareStatement(sql);
                ps.setString(1, search.getBookingDate());
                ps.setInt(2, search.getCourtType());
                ps.setString(3, search.getStartTime());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {

                    int courtNumber = rs.getInt("courtNumber");
                    String courtName = rs.getString("courtName");
                    int courtType = rs.getInt("courtType");
                    String bookingDate = rs.getString("bookingDate");
                    String startTime = rs.getString("startTime");
                    System.out.println("courtInfoSaved");
                    courts.add(new CourtSearchFound(courtNumber, courtName, courtType, bookingDate, startTime));
                }
            }

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        //returns all court
        return courts;
    }
}
