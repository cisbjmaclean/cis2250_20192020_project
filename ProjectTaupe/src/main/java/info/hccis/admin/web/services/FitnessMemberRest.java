package info.hccis.admin.web.services;

import com.google.gson.Gson;
import info.hccis.admin.data.springdatajpa.FitnessMemberRepository;
import info.hccis.admin.model.entity.Fitnessmember;
import java.io.IOException;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@Path("/MemberService")
public class FitnessMemberRest {

    @Resource
    private final FitnessMemberRepository fmr;

    /**
     * 
     * @param servletContext 
     */
    public FitnessMemberRest(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.fmr = applicationContext.getBean(FitnessMemberRepository.class);
    }

    /**
     * This rest service will provide one fitness member from the associated database.
     *
     * @param id
     * @return String
     * @since 20191202
     * @author mblanchard
     */
    @GET
    @Path("/member/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getFitnessMember(@PathParam("id") int id) {

        Fitnessmember fitnessMember = fmr.findOne(id);
        Gson gson = new Gson();
        return gson.toJson(fitnessMember);

    }
    
    /**
     * This rest service will provide all fitness members from the associated database.
     *
     * @param id
     * @return String
     * @since 20191202
     * @author mblanchard
     */
    @GET
    @Path("/member")
    @Produces(MediaType.APPLICATION_JSON)
    public String getFitnessMembers() {

        Iterable<Fitnessmember> fitnessMembers = fmr.findAll();
        Gson gson = new Gson();
        return gson.toJson(fitnessMembers);

    }

    /**
     * POST operation which will update a camper.
     * @param jsonIn 
     * @return response including the json representing the new fitnessMember.
     * @throws IOException 
     */
    @POST
    @Path("/member")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String updateFitnessMember(String jsonIn) throws IOException {
        
        Gson gson = new Gson();
        Fitnessmember fitnessMember = gson.fromJson(jsonIn, Fitnessmember.class);
        fitnessMember = fmr.save(fitnessMember);
        
        return gson.toJson(fitnessMember);

    }
    

    
    
    
    
}
