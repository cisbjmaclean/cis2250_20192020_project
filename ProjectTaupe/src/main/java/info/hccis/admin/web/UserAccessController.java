package info.hccis.admin.web;

import info.hccis.admin.dao.CourtBookingDAO;
import info.hccis.admin.data.springdatajpa.UserAccessRepository;
import info.hccis.admin.model.entity.Courtbooking;
import info.hccis.admin.model.entity.Useraccess;
import java.util.ArrayList;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 */
@Controller
@RequestMapping("/userAccess")
public class UserAccessController {

    private final UserAccessRepository uar;

    @Autowired
    public UserAccessController(UserAccessRepository uar) {
        this.uar = uar;
    }

    @RequestMapping("/search")
    public String searchBookingTimes(Model model, HttpSession session) {
        Iterable<Useraccess> userAccesses = uar.findAll();
        model.addAttribute("userAccesses", userAccesses);
        return "userAccess/search";
    }

    @RequestMapping("/results")
    public String returnSearchResults(@RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate,
            @RequestParam("member") String memberString, Model model) {
        int member = Integer.parseInt(memberString);
        endDate = endDate.replace("-", "");
        startDate = startDate.replace("-", "");

        ArrayList<Courtbooking> courtBookings = CourtBookingDAO.searchForBookingByDateRange(member, startDate, endDate);

        if (courtBookings.size() > 0) {
            model.addAttribute("courtBookings", courtBookings);

            return "userAccess/results";
        } else {
            model.addAttribute("message", "No court bookings found in the submitted range for this member.");
            Iterable<Useraccess> userAccesses = uar.findAll();
            model.addAttribute("userAccesses", userAccesses);
            return "userAccess/search";
        }
    }

}
