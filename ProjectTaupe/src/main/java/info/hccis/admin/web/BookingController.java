package info.hccis.admin.web;

import info.hccis.admin.bo.CourtbookingBO;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import info.hccis.admin.data.springdatajpa.BookingRepository;
import info.hccis.admin.data.springdatajpa.CourtRepository;
import info.hccis.admin.data.springdatajpa.CourttimesRepository;
import info.hccis.admin.data.springdatajpa.FitnessMemberRepository;
import info.hccis.admin.data.springdatajpa.UserAccessRepository;
import info.hccis.admin.model.entity.Courtbooking;
import info.hccis.admin.model.entity.Fitnessmember;
import info.hccis.admin.model.entity.Useraccess;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;

/*
Author: Alex Creamer
Date: 2019/12/05
Purpose: Controller class to receive data from View layer and process data which is then sent to database using JPA.
*/
@Controller
public class BookingController {

    //Declare Repository objects to be used in request mappings
    private final BookingRepository br;
    private final CourtRepository cr;
    private final CourttimesRepository ctr;
    private final UserAccessRepository ur;
    private final FitnessMemberRepository fr;

    //Constructor 
    @Autowired
    public BookingController(BookingRepository br, CourtRepository cr, CourttimesRepository ctr,UserAccessRepository ur,FitnessMemberRepository fr) {
        this.br = br;
        this.cr = cr;
        this.ctr = ctr;
        this.ur = ur;
        this.fr = fr;
    }

    /*
    Author: Alex Creamer
    Date: 2019/12/05
    Purpose: When export courtbookings link is clicked CSV file is created using method from Courtbooking Business Object and save to users machine.
    Redirects user to success page to confirm.
    */
    @RequestMapping("/courtBooking/export")
    public String exportBooking(Model model, HttpServletRequest request) {
        ArrayList<Courtbooking> bookings = new ArrayList();
        String name = "bookings";
        for (Courtbooking booking : br.findAll()) {
            bookings.add(booking);
        }
        CourtbookingBO.exportToCsv(bookings, name);

        return "courtBookings/exportSuccess";
    }

    /*
    Author: Alex Creamer
    Date: 2019/12/05
    Purpose: Set up model with objects to display information for when booking a court.
    */
    @RequestMapping("/courtBooking/book")
    public String book(Model model, HttpServletRequest request) {
        Courtbooking booking = new Courtbooking();
        model.addAttribute("courts", cr.findAll());
        model.addAttribute("courtTimes", ctr.findAll());
        model.addAttribute("members", ur.findAll());
        model.addAttribute("booking", booking);
        return "courtBookings/book";
    }

      /*
    Author: Alex Creamer
    Date: 2019/12/05
    Purpose:When form is submitted on booking court page this method is called to process data sent and save court booking to database
    */
    @RequestMapping("/courtBooking/addCourtBooking")
    public String addCourtBooking(Model model, HttpSession session, @ModelAttribute("booking") Courtbooking booking, BindingResult result, @RequestParam("bookingDate") String bookingDate) {
        //Declare strings to store error messages if validation is not passed
        boolean memberMessage = false;
        boolean courtMessage = false;
        //Declare variable to keep track of amount of booking member has
        int memberCount = 0;
        //Loop through each booking in database
        for (Courtbooking item : br.findAll()) {
            //Check if member booking court has already booked a court
            if (item.getMemberId() == booking.getMemberId()) {
                memberCount++;
                //If member has booked two courts set error message
                if (memberCount == 2) {
                    memberMessage = true;
                }
            }
            //Check if court being booked has already been booked
            if (item.getCourtNumber() == booking.getCourtNumber()) {
                //If court has been booked on the same day and time set error message
                if (item.getStartTime().equals(booking.getStartTime()) && item.getBookingDate().equals(bookingDate.replaceAll("-", ""))) {
                    courtMessage = true;
                }
            }
        }
        //If both error messages are empty validation has passed
        if (courtMessage == false && memberMessage == false) {
            try {
                //Create data object for time of court being booked to add to database
                LocalDateTime now = LocalDateTime.now();
                //Set object's CreatedDate using setter and covert date object to proper format 
                booking.setCreatedDate(now.toLocalDate().toString().replaceAll("-", ""));
                //Format booking date to proper format and set using setter
                booking.setBookingDate(bookingDate.replaceAll("-", ""));
                //Save object to database
                br.save(booking);
            } catch (Exception ex) {
                Logger.getLogger(Courtbooking.class.getName()).log(Level.SEVERE, null, ex);
            }
            //Add proper elements to model and return to list view of court bookings
            model.addAttribute("bookings", br.findAll());
            model.addAttribute("members", ur.findAll());
            return "courtBookings/list";
        } else {
            //If validation did not pass add proper elements to model and return to booking page with error messages informing user
            Courtbooking bookingToAdd = new Courtbooking();
            model.addAttribute("courts", cr.findAll());
            model.addAttribute("courtTimes", ctr.findAll());
            model.addAttribute("members", ur.findAll());
            model.addAttribute("booking", bookingToAdd);
            model.addAttribute("memberMessage", memberMessage);
            model.addAttribute("courtMessage", courtMessage);
            return "courtBookings/book";
        }
    }
    
    /*
    Author: Alex Creamer
    Date: 2019/12/05
    Purpose:Sets up model for list view page and redirects user when link is clicked
    */
    @RequestMapping("/courtBooking/list")
    public String list(Model model, HttpServletRequest request) {
        model.addAttribute("members", ur.findAll());
        model.addAttribute("bookings", br.findAll());
        return "courtBookings/list";
    }

     /*
    Author: Alex Creamer
    Date: 2019/12/05
    Purpose:Sets up model for member usage report page and redirects user when link is clicked
    */
    @RequestMapping("/courtBooking/getReport")
    public String getReport(Model model, HttpServletRequest request) {
        CourtbookingBO booking = new CourtbookingBO();
        model.addAttribute("booking", booking);
        return "fitnessMembers/getReport";
    }

    /*
    Author: Alex Creamer
    Date: 2019/12/05
    Purpose: Process data from user for report and redirect them to view of report.
    */
    @RequestMapping("/courtBooking/displayReport")
    public String displayReport(Model model, HttpServletRequest request, @RequestParam("endDate") String endDate, @RequestParam("startDate") String startDate) {
        //Declare Array Lists to be passed to view model
        ArrayList<Courtbooking> bookings = new ArrayList();
        ArrayList<Fitnessmember> members = new ArrayList();
        //Create hashmap to associate member with number of court bookings
        HashMap<Integer, Integer> bookingsCount = new HashMap<Integer, Integer>();
        //Declare data format to be used
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        try {
            //Format user dates from date picker into correct format
            Date userStartDate = format.parse(startDate.replaceAll("-", ""));
            Date userEndDate = format.parse(endDate.replaceAll("-", ""));
            //Loop through each booking in database
            for (Courtbooking item : br.findAll()) {
                //Create booking object from each bookings booking date to compare to users input
                Date bookingDate = format.parse(item.getBookingDate());
                //If date falls within range add to bookings Array List
                if (bookingDate.after(userStartDate) && bookingDate.before(userEndDate) || bookingDate.equals(userStartDate) || bookingDate.equals(userEndDate)) {
                    bookings.add(item);
                }
            }
        } catch (ParseException exception) {
            System.out.println(exception);
        }
        //Loop through each fitness member in database
        for (Fitnessmember member : fr.findAll()) {
            int count = 0;
            //Lopp through each booking in database
            for (Courtbooking item : bookings) {
                //If booking member Id and fitness member Id match add to count variable
                if (item.getMemberId() == member.getUserId()) {
                    count++;
                }
            }
            //Add each member to hashmap associated with number of bookings they have in database
            bookingsCount.put(member.getUserId(), count);
            members.add(member);
        }
        //If bookings is empty return error message
        if (bookings.isEmpty()) {  
            model.addAttribute("bookings", bookings);
            model.addAttribute("startDate", startDate);
            model.addAttribute("endDate", endDate);
        } else {
            //If bookings exist return to display report page with appropirate elements
            model.addAttribute("startDate", startDate);
            model.addAttribute("endDate", endDate);
            model.addAttribute("bookingsCount", bookingsCount);
            model.addAttribute("members", members);
            model.addAttribute("bookings", bookings);
        }
        return "fitnessMembers/displayReport";
    }

}
