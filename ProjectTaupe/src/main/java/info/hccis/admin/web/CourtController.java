package info.hccis.admin.web;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import info.hccis.admin.dao.CodeTypeDAO;
import info.hccis.admin.dao.CodeValueDAO;
import info.hccis.admin.dao.CourtDAO;
import info.hccis.admin.dao.CourtSearchDAO;
import info.hccis.admin.data.springdatajpa.BookingRepository;
import info.hccis.admin.data.springdatajpa.CodeValueRepository;
import info.hccis.admin.data.springdatajpa.CourtRepository;
import info.hccis.admin.data.springdatajpa.CourttimesRepository;
import info.hccis.admin.data.springdatajpa.FitnessMemberRepository;
import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.entity.CourtInformation;
import info.hccis.admin.model.entity.CourtSearch;
import info.hccis.admin.model.entity.Courtbooking;
import info.hccis.admin.model.entity.Courttimes;
import info.hccis.admin.model.jpa.CodeType;
import info.hccis.admin.model.jpa.CodeValue;
import info.hccis.admin.util.Utility;
import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 * Controller for the court entity
 * @author Brandon
 * @since Dec 11 2019
 */
@Controller
public class CourtController {
    private final CourtRepository cr;
    private final CodeValueRepository cvr;
    private final CourttimesRepository ctr;
    /**
     * method with repositories
     *
     * @author Brandon
     * @since Dec 11 2019
     */
    @Autowired
    public CourtController(CourtRepository cr,CodeValueRepository cvr,CourttimesRepository ctr) {
        this.cr = cr;
        this.cvr = cvr;
        this.ctr=ctr;
    }
    /**
     * method lists the courts, then sends the user to court list page
     *
     * @author Brandon
     * @since Dec 11 2019
     */
    @RequestMapping("/court/list")
    public String courtList(Model model) {

        //Get the courts from the database
        model.addAttribute("Courts", CourtDAO.selectAll());
        
        //This will send the user to the welcome.html page.
        return "court/list";
    }
    /**
     * method for adding a court, which sends user to court add page
     *
     * @author Brandon
     * @since Dec 11 2019
     */
     @RequestMapping("/court/add")
    public String courtAdd(Model model,HttpSession session) {
        //creates new court object
        CourtInformation newCourt = new CourtInformation();
        //adds to the model, the new court and the codevalues for the courts
        model.addAttribute("Court", newCourt);
        model.addAttribute("codeValues", cvr.findByCodeTypeId(2));
        //This will send the user to the add court page.
        return "court/add";
    }
    /**
     * method for updating a court, which sends user to court add page with the courts information
     *
     * @author Brandon
     * @since Dec 11 2019
     */
    @RequestMapping("/court/update")
    public String courtUpdate(Model model, HttpServletRequest request) {

        //Gets the court Id
        String idToFind = request.getParameter("id");
        System.out.println("BJM id passed=" + idToFind);
        //Finds the court which matchs the id  
        CourtInformation editCourt = CourtDAO.select(Integer.parseInt(idToFind));
        //adds to the model, the found court and the codevalues for the courts
        model.addAttribute("codeValues", cvr.findByCodeTypeId(2));
        model.addAttribute("Court", editCourt);
        //sends the user to the court add page
        return "/court/add";

    }
    /**
     * method for deleting a court, which afterwards sends user to court list page
     * @author Brandon
     * @since Dec 11 2019
     */
    @RequestMapping("/court/delete")
    public String courtDelete(Model model, HttpServletRequest request) {
        //Gets the court ID
        String idToFind = request.getParameter("id");
        System.out.println("BJM id passed=" + idToFind);
        try {
            //Delets from database based on id
            CourtDAO.delete(Integer.parseInt(idToFind));
        } catch (Exception ex) {
            System.out.println("Could not delete");
        }

        //Reload the court list and sends the user back to court list page.
        model.addAttribute("Courts", CourtDAO.selectAll());
        return "/court/list";

    }
    /**
     * method for adding a court into a database, which afterwards sends user to court list page
     * @author Brandon
     * @since Dec 11 2019
     */
    @RequestMapping("/court/addSubmit")
    public String courtAddSubmit(Model model, @Valid @ModelAttribute("Court") CourtInformation theCourtFromTheForm, BindingResult result) {
        //Adds/updates the court into the database
        System.out.println(theCourtFromTheForm.toString());
        try {
            CourtDAO.update(theCourtFromTheForm);
        } catch (Exception e) {
            System.out.println("Could not save to the database");
        }

        System.out.println("BJM-Did we get here2?");
        //Reload the court list
        model.addAttribute("Courts", CourtDAO.selectAll());
        //This will send the user to the court list page.
        return "court/list";
    }
    /**
     * method for heading to the search page, which sends the user to the 
     * searchCourts page
     * 
     * @author Brandon
     * @since Dec 11 2019
     */
    @RequestMapping("/court/search")
    public String courtSearch(Model model) {
        //creates new court search object
        CourtSearch newCourt = new CourtSearch();
        //adds the court search object, codevalues for courts and all the court times
        model.addAttribute("codeValues", cvr.findByCodeTypeId(2));
        model.addAttribute("courtTimes", ctr.findAll());
        model.addAttribute("Court", newCourt);

        //This will send the user to the welcome.html page.
        return "court/searchCourts";
    }
    /**
     * method for searching the database for the courts which match the search 
     * parameters, then sends the user to see the results in the displayAvailableCourt page
     * 
     * @author Brandon
     * @since Dec 11 2019
     */
    @RequestMapping("/court/findCourts")
    public String courtsFound(Model model,@Valid @ModelAttribute("Court") CourtSearch court, BindingResult result) throws ParseException {
        //Selects all courts which match the search parameters 
        model.addAttribute("Courts", CourtSearchDAO.selectAll(court));
        //sends the user to the displayAvailableCourt page
        return "court/displayAvailableCourt";
    }
    /**
     * method for exporting the court data from the database and which sends the user to the 
     * confirmation page
     * 
     * @author Brandon
     * @since Dec 11 2019
     */
    @RequestMapping("court/export")
    public String export(Model model, HttpServletRequest request) {
        //Gets all the court information
        System.out.println(request);
        ArrayList<CourtInformation> courtInformation = CourtDAO.selectAll();
        System.out.println(courtInformation.toString());
        model.addAttribute("courts", courtInformation);
        //name of textfile with time it was created
        String filePath = System.getProperty("user.home") + "\\fitness\\court_"
                + Utility.getNow("yyyyMMddhhmmss") + ".CSV";
        File file = new File(filePath);
        file.getParentFile().mkdirs();

        try (
                //writes information to the textfile
                Writer writer = Files.newBufferedWriter(Paths.get(filePath));) {
            StatefulBeanToCsv<CourtInformation> beanToCsv = new StatefulBeanToCsvBuilder(writer)
                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER).build();
            beanToCsv.write(courtInformation);
        } catch (IOException ex) {
            Logger.getLogger(CourtController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CsvDataTypeMismatchException ex) {
            Logger.getLogger(CourtController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CsvRequiredFieldEmptyException ex) {
            Logger.getLogger(CourtController.class.getName()).log(Level.SEVERE, null, ex);
        }
        //Sends user to the confirmation page
        return "court/export";
    }

}
