package info.hccis.admin.web.services;

import java.util.ArrayList;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@Path("/api")
public class ApiService {

    public ApiService(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
    }

    @GET
    @Path("/")
    @Produces(MediaType.TEXT_HTML)
    public Response getServices() {
        ArrayList<String> services = new ArrayList();

        String output = "<html><body><h1>Services available</h1>";
        output += "<h2>UserAccessService</h2>";
        output += "<p>Get all users</p>";
        output += "<p>example:   <a target=\"_blank\" href=\"http://hccis.info:8080/court/rest/UserAccessService/user\">http://hccis.info:8080/court/rest/UserAccessService/user</a></p>";
        output += "<p>Login a user</p>";
        output += "<p>Note the db, user, pw are passed comma separated and the user type is returned or 0 if not valid</p>";
        output += "<p>example:   <a target=\"_blank\" href=\"http://hccis.info:8080/court/rest/UserAccessService/login/cis2232_fitness,bj.maclean@gmail.com,123\">http://hccis.info:8080/court/rest/UserAccessService/user</a></p>";

        output += "<h2>UserAccessService</h2>";
        output += "<p>Get all users</p>";
        output += "<p>example:   <a target=\"_blank\" href=\"http://hccis.info:8080/court/rest/UserAccessService/user\">http://hccis.info:8080/court/rest/UserAccessService/user</a></p>";
        output += "<p>Login a user</p>";
        output += "<p>Note the db, user, pw are passed comma separated and the user type is returned or 0 if not valid</p>";
        output += "<p>example:   <a target=\"_blank\" href=\"http://hccis.info:8080/court/rest/UserAccessService/login/cis2232_fitness,bj.maclean@gmail.com,123\">http://hccis.info:8080/court/rest/UserAccessService/login/cis2232_fitness,bj.maclean@gmail.com,123</a></p>";
        
        output += "<h2>CodeService</h2>";
        output += "<p>Get code values</p>";
        output += "<p>example:   <a target=\"_blank\" href=\"http://hccis.info:8080/court/rest/CodeService/values/cis2232_fitness/2\">http://hccis.info:8080/court/rest/CodeService/values/cis2232_fitness/2</a></p>";

        output += "<h2>MemberService</h2>";
        output += "<p>Get all members</p>";
        output += "<p>example:   <a target=\"_blank\" href=\"http://hccis.info:8080/court/rest/MemberService/member\">http://hccis.info:8080/court/rest/MemberService/member</a></p>";
        output += "<p>Get specific fitness member (pass id to the url)</p>";
        output += "<p>example:   <a target=\"_blank\" href=\"http://hccis.info:8080/court/rest/MemberService/member/1\">http://hccis.info:8080/court/rest/MemberService/member/1</a></p>";

        output += "<h2>BookingService</h2>";
        output += "<p>Get all bookings</p>";
        output += "<p>example:   <a target=\"_blank\" href=\"http://hccis.info:8080/court/rest/BookingService/booking\">http://hccis.info:8080/court/rest/BookingService/booking</a></p>";
        output += "<p>Get specific booking (pass id to the url)</p>";
        output += "<p>example:   <a target=\"_blank\" href=\"http://hccis.info:8080/court/rest/BookingService/booking/1\">http://hccis.info:8080/court/rest/BookingService/booking/1</a></p>";

        output += "<h2>CourtService</h2>";
        output += "<p>Get all courts</p>";
        output += "<p>example:   <a target=\"_blank\" href=\"http://hccis.info:8080/court/rest/CourtService/court\">http://hccis.info:8080/court/rest/CourtService/court</a></p>";
        output += "<p>Get specific court (pass id to the url)</p>";
        output += "<p>example:   <a target=\"_blank\" href=\"http://hccis.info:8080/court/rest/CourtService/court/1\">http://hccis.info:8080/court/rest/CourtService/court/1</a></p>";

        return Response.status(200).entity(output).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
    }
}
