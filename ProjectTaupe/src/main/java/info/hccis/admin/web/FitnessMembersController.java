/*
 * This is the controller for the fitnessMember entity.
 */
package info.hccis.admin.web;

import com.google.common.collect.Lists;
import info.hccis.admin.model.entity.Fitnessmember;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import info.hccis.admin.dao.CodeValueDAO;
import info.hccis.admin.data.springdatajpa.FitnessMemberRepository;
import info.hccis.admin.data.springdatajpa.UserAccessRepository;
import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.util.Utility;
import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

/**
 *
 * @author mblanchard9255
 */
@RequestMapping("/fitnessMembers")
@Controller
public class FitnessMembersController {

    private final FitnessMemberRepository fmr;
    private final UserAccessRepository uar;

    @Autowired
    public FitnessMembersController(FitnessMemberRepository fmr, UserAccessRepository uar) {
        this.fmr = fmr;
        this.uar = uar;
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping("/export")
    public String export(Model model, HttpServletRequest request) {
        System.out.println(request);
        Iterable<Fitnessmember> fitnessMembers = fmr.findAll();
        System.out.println(fitnessMembers.toString());
        model.addAttribute("fitnessMembers", fitnessMembers);

        String filePath = System.getProperty("user.home") + "\\fitness\\member_"
                + Utility.getNow("yyyyMMddhhmmss") + ".CSV";
        File file = new File(filePath);
        file.getParentFile().mkdirs();

        ArrayList<Fitnessmember> fitnessMembersList = Lists.newArrayList(fitnessMembers);
        
        try (
                
                Writer writer = Files.newBufferedWriter(Paths.get(filePath));) {
            StatefulBeanToCsv<Fitnessmember> beanToCsv = new StatefulBeanToCsvBuilder(writer)
                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER).build();
            beanToCsv.write(fitnessMembersList);
        } catch (IOException | CsvDataTypeMismatchException | CsvRequiredFieldEmptyException ex) {
            Logger.getLogger(FitnessMembersController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "fitnessMembers/export";
    }

    /**
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping("/list")
    public String showFitnessMembers(Model model, HttpServletRequest request) {
        Iterable<Fitnessmember> fitnessMembers = fmr.findAll();
        for (Fitnessmember current : fitnessMembers) {
            current.setStatusDescription(CodeValueDAO.getCodeValueDescription(new DatabaseConnection(), 3, current.getStatus()));
        }
        model.addAttribute("fitnessMembers", fitnessMembers);
        return "fitnessMembers/list";
    }
    
    /**
     * 
     * @param model
     * @return 
     */
    @RequestMapping("/add")
    public String addFitnessMember(Model model) {
        Fitnessmember fitnessMember = new Fitnessmember();
        model.addAttribute("fitnessMember", fitnessMember);
        model.addAttribute("title", "Add New Member");
        return "fitnessMembers/add";
    }
    
    /**
     * 
     * @param model
     * @param id
     * @return 
     */
    @RequestMapping("/edit/{id}")
    public String edit(Model model, @PathVariable int id) {
        //This will send the user to the welcome.html page.
        Fitnessmember fitnessMember = fmr.findOne(id);
        model.addAttribute("fitnessMember", fitnessMember);
        model.addAttribute("title", "Update Existing Member");
        
        return "fitnessMembers/add";
    }
    
    /**
     * 
     * @param model
     * @param session
     * @param fitnessMember
     * @param result
     * @return 
     */
    @RequestMapping("/addSubmit")
    public String addSubmit(Model model, HttpSession session, @Valid @ModelAttribute("fitnessMember") Fitnessmember fitnessMember, BindingResult result) {

        if (result.hasErrors()) {
            System.out.println("Error in validation.");
            String error = "Validation Error";
            model.addAttribute("message", error);
            return "fitnessMembers/add";
        }
        
        if (uar.findOne(fitnessMember.getUserId()) == null) {
            System.out.println("No user account associated with this ID.");
            String error = "User account required.";
            model.addAttribute("message", error);
            return "fitnessMembers/add";
        }

        try {
            fmr.save(fitnessMember);
        } catch (Exception ex) {
            Logger.getLogger(FitnessMembersController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Iterable<Fitnessmember> fitnessMembers = fmr.findAll();
        model.addAttribute("fitnessMembers", fitnessMembers);

        return "redirect:/fitnessMembers/list";
    }
    
    /**
     * 
     * @param model
     * @param id
     * @return 
     */
    @RequestMapping("/delete/{id}")
    public String delete(Model model, @PathVariable int id) {

        //This will send the user to the welcome.html page.
        fmr.delete(id);
        Iterable<Fitnessmember> fitnessMembers = fmr.findAll();
        model.addAttribute("fitnessMembers", fitnessMembers);

        return "redirect:/fitnessMembers/list";
    }    
}
