package info.hccis.admin.web.services;

import com.google.gson.Gson;
import info.hccis.admin.data.springdatajpa.BookingRepository;
import info.hccis.admin.model.entity.Courtbooking;
import java.io.IOException;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

//issues here...
//http://www.scriptscoop.net/t/ba4553fde8ca/spring-autowired-classes-are-null-in-jersey-rest.html

@Component
@Path("/BookingService")
@Scope("request")
public class BookingService {

    @Resource
    private final BookingRepository br;

    
    public BookingService(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.br = applicationContext.getBean(BookingRepository .class);
    }
    
    @GET
    @Path("/booking")
    public Response getAllBookings() {

        ArrayList<Courtbooking> bookings = new ArrayList<>();
        for (Courtbooking courtbooking :  br.findAll()) {
            bookings.add(courtbooking);
        }
         Gson gson = new Gson();

        return Response.status(200).entity(gson.toJson(bookings)).build();

    }
    
     @GET
    @Path("/booking/{param}")
    public Response getBookings(@PathParam("param") int bookingId) {

        ArrayList<Courtbooking> bookings = new ArrayList<>();
        for (Courtbooking courtbooking :  br.findAll()) {
            if(bookingId == courtbooking.getId()){
             bookings.add(courtbooking);   
            }
        }
         Gson gson = new Gson();

        return Response.status(200).entity(gson.toJson(bookings)).build();

    }
    
    @POST
    @Path("/booking/add")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addBooking(String jsonIn) throws IOException{
        Gson gson = new Gson();
        Courtbooking booking = gson.fromJson(jsonIn, Courtbooking.class);
        booking = br.save(booking);
        String jsonString = "";
        jsonString = gson.toJson(booking);
        return Response.status(201).entity(jsonString).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
    } 
    

}
