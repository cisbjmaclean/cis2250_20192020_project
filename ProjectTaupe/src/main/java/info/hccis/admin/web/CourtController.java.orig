/*
 * This is the controller for the court entity.
 */
package info.hccis.admin.web;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import info.hccis.admin.dao.CodeTypeDAO;
import info.hccis.admin.dao.CodeValueDAO;
import info.hccis.admin.dao.CourtDAO;
import info.hccis.admin.data.springdatajpa.BookingRepository;
import info.hccis.admin.data.springdatajpa.CodeValueRepository;
import info.hccis.admin.data.springdatajpa.CourtRepository;
import info.hccis.admin.data.springdatajpa.CourttimesRepository;
import info.hccis.admin.data.springdatajpa.FitnessMemberRepository;
import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.entity.CourtInformation;
import info.hccis.admin.model.entity.Courttimes;
import info.hccis.admin.model.jpa.CodeType;
import info.hccis.admin.model.jpa.CodeValue;
import info.hccis.admin.util.Utility;
import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 *
 * @author Brandon
 */
@Controller
public class CourtController {
    private final CourtRepository cr;
    private final CodeValueRepository cvr;
    private final CourttimesRepository ctr;
    
    @Autowired
    public CourtController(CourtRepository cr,CodeValueRepository cvr,CourttimesRepository ctr) {
        this.cr = cr;
        this.cvr = cvr;
        this.ctr=ctr;
    }
    
    @RequestMapping("/court/list")
    
    public String courtList(Model model) {

        //Get the OJT Student from the database
        model.addAttribute("Courts", CourtDAO.selectAll());
        
        //This will send the user to the welcome.html page.
        return "court/list";
    }
     @RequestMapping("/court/add")
    public String courtAdd(Model model,HttpSession session) {

        //put a reflection object in the model to be used to associate with the input tags of 
        //the form.
        CourtInformation newCourt = new CourtInformation();
        ArrayList<CodeValue> codeValues = new ArrayList();
        
        int i=0;
        for (CodeValue codeValue : cvr.findByCodeTypeId(2)) {
            codeValues.add(codeValue);
            System.out.println(codeValue.getEnglishDescription());
        }
        model.addAttribute("Court", newCourt);
        model.addAttribute("codeValues", codeValues);
        //This will send the user to the welcome.html page.
        return "court/add";
    }
    @RequestMapping("/court/update")
    public String courtUpdate(Model model, HttpServletRequest request) {

        //- This method will use the registration id which is passed as a request 
        //parameter.  
        String idToFind = request.getParameter("id");
        System.out.println("BJM id passed=" + idToFind);
        //- It will go to the database and load the OJT Student details into a camper
        //  object and put that object in the model.  
        CourtInformation editCourt = CourtDAO.select(Integer.parseInt(idToFind));
         ArrayList<CodeValue> codeValues = new ArrayList();
        for (CodeValue codeValue : cvr.findByCodeTypeId(2)) {
            codeValues.add(codeValue);
            System.out.println(codeValue.getEnglishDescription());
        }
        model.addAttribute("codeValues", codeValues);
        model.addAttribute("Court", editCourt);
        return "/court/add";

    }

    @RequestMapping("/court/delete")
    public String courtDelete(Model model, HttpServletRequest request) {

        //- This method will use the registration id which is passed as a request 
        //parameter.  
        String idToFind = request.getParameter("id");
        System.out.println("BJM id passed=" + idToFind);
        try {
            //- It will go to the database and load the OJT Student details into a camper
            //  object and put that object in the model.
            CourtDAO.delete(Integer.parseInt(idToFind));
        } catch (Exception ex) {
            System.out.println("Could not delete");
        }

        //Reload the campers list so it can be shown on the next view.
        model.addAttribute("Courts", CourtDAO.selectAll());
        return "/court/list";

    }

    @RequestMapping("/court/addSubmit")
    public String courtAddSubmit(Model model, @Valid @ModelAttribute("Court") CourtInformation theCourtFromTheForm, BindingResult result) {
        //Call the dao method to put this guy in the database.
        System.out.println(theCourtFromTheForm.toString());
        try {
            CourtDAO.update(theCourtFromTheForm);
        } catch (Exception e) {
            System.out.println("Could not save to the database");
        }

        System.out.println("BJM-Did we get here2?");
        //Reload the OJT Student list so it can be shown on the next view.
        model.addAttribute("Courts", CourtDAO.selectAll());
        //This will send the user to the welcome.html page.
        return "court/list";
    }
    @RequestMapping("/court/search")
    public String courtSearch(Model model) {
            ArrayList<CodeValue> codeValues = new ArrayList();
        for (CodeValue codeValue : cvr.findByCodeTypeId(2)) {
            codeValues.add(codeValue);
            System.out.println(codeValue.getEnglishDescription());
        }
        ArrayList<Courttimes> courtTimes = new ArrayList();
        for (Courttimes time : ctr.findAll()) {
            courtTimes.add(time);
        }
        //model.addAttribute("Court", newCourt);
        model.addAttribute("codeValues", codeValues);
        model.addAttribute("courtTimes", courtTimes);
        //This will send the user to the welcome.html page.
        return "court/searchCourts";
    }
    @RequestMapping("court/export")
    public String export(Model model, HttpServletRequest request) {
        System.out.println(request);
        ArrayList<CourtInformation> courtInformation = CourtDAO.selectAll();
        System.out.println(courtInformation.toString());
        model.addAttribute("courts", courtInformation);

        String filePath = System.getProperty("user.home") + "\\fitness\\court_"
                + Utility.getNow("yyyyMMddhhmmss") + ".CSV";
        File file = new File(filePath);
        file.getParentFile().mkdirs();

        try (
                Writer writer = Files.newBufferedWriter(Paths.get(filePath));) {
            StatefulBeanToCsv<CourtInformation> beanToCsv = new StatefulBeanToCsvBuilder(writer)
                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER).build();
            beanToCsv.write(courtInformation);
        } catch (IOException ex) {
            Logger.getLogger(CourtController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CsvDataTypeMismatchException ex) {
            Logger.getLogger(CourtController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CsvRequiredFieldEmptyException ex) {
            Logger.getLogger(CourtController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "court/export";
    }
<<<<<<< HEAD

}
=======
}
>>>>>>> origin/master
