package info.hccis.admin.web.services;

import com.google.gson.Gson;
import info.hccis.admin.data.springdatajpa.CourtRepository;
import info.hccis.admin.model.entity.CourtInformation;
import java.io.IOException;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;
     /**
     * Rest service for courts
     *
     * @author Brandon
     * @since Dec 11 2019
     */
@Component
@Path("/CourtService")
@Scope("request")
public class CourtService {

    @Resource
    private final CourtRepository cr;

    
    public CourtService(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.cr = applicationContext.getBean(CourtRepository .class);
    }
    /**
     * Method for getting all the courts
     *
     * @author Brandon
     * @since Dec 11 2019
     */
    @GET
    @Path("/court")
    public Response getAllCourts() {

        ArrayList<CourtInformation> courts = new ArrayList<>();
        for (CourtInformation court :  cr.findAll()) {
            courts.add(court);
        }
         Gson gson = new Gson();

        return Response.status(200).entity(gson.toJson(courts)).build();

    }
    /**
     * returns one court with the id that is entered
     *
     * @author Brandon
     * @since Dec 11 2019
     */
    @GET
    //@Produces({MediaType.APPLICATION_JSON}) 
    @Path("court/{param}")
    public Response getOneCourt(@PathParam("param") String courtId) {

         CourtInformation court = cr.findOne(Integer.parseInt(courtId));

        if (court == null) {
            return Response.status(204).entity("{}").build();
        } else {
            Gson gson = new Gson();
            return Response.status(200).entity(gson.toJson(court)).build();
        }

    }
    /**
     * Adds a new court
     *
     * @author Brandon
     * @since Dec 11 2019
     */
    @POST
    @Path("/court/add")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addCourt(String jsonIn) throws IOException{
        Gson gson = new Gson();
        CourtInformation court = gson.fromJson(jsonIn, CourtInformation.class);
        court = cr.save(court);
        String jsonString = "";
        jsonString = gson.toJson(court);
        return Response.status(201).entity(jsonString).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
    } 
    

}
