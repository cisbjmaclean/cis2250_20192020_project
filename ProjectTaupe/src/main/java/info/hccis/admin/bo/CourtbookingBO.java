/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.bo;

import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import info.hccis.admin.util.Utility;
import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import org.eclipse.persistence.internal.oxm.schema.model.Element;

/**
 *
 * @author acreamer110186
 */
public class CourtbookingBO {

    public static void exportToCsv(ArrayList list, String name) {
        String filePath = System.getProperty("user.home") + "\\fitness\\" + name + "_" + Utility.getNow("yyyyMMddhhmmss") + ".csv";
        File file = new File(filePath);
        file.getParentFile().mkdirs();
        try {
            Writer writer = Files.newBufferedWriter(Paths.get(filePath));

            {
                StatefulBeanToCsv<Element> beanToCsv = new StatefulBeanToCsvBuilder(writer)
                        .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                        .build();

                beanToCsv.write(list);

            }
            writer.close();
        } catch (IOException e) {
            System.out.println(e);
        } catch (CsvDataTypeMismatchException e) {
            System.out.println(e);
        } catch (CsvRequiredFieldEmptyException e) {
            System.out.println(e);
        }
    }
    
   
}
