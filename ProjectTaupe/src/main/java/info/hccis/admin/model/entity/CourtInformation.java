package info.hccis.admin.model.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Court Information entity
 * @author Brandon
 * @since Dec 11 2019
 */
@Entity
@Table(name = "Court")
@XmlRootElement
public class CourtInformation implements Serializable {
    //List of variables
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer courtId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "courtNumber")
    private int courtNumber;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "courtName")
    private String courtName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "courtType")
    private int courtType;
    /**
     * Default Constructor
     *
     * @author Brandon
     * @since Dec 11 2019
     */
    public CourtInformation() {
    }
    /**
     * Constructor with just the id entered
     *
     * @author Brandon
     * @since Dec 11 2019
     */
    public CourtInformation(Integer id) {
        this.courtId = id;
    }
    /**
     * Constructor with id,courtNumber,courtName and courtType
     *
     * @author Brandon
     * @since Dec 11 2019
     */
    public CourtInformation(Integer id, int courtNumber, String courtName, int courtType) {
        this.courtId = id;
        this.courtNumber = courtNumber;
        this.courtName = courtName;
        this.courtType = courtType;
    }
    //List of getters and setters
    public Integer getCourtId() {
        return courtId;
    }

    public void setCourtId(Integer courtId) {
        this.courtId = courtId;
    }

    public int getCourtNumber() {
        return courtNumber;
    }

    public void setCourtNumber(int courtNumber) {
        this.courtNumber = courtNumber;
    }

    public String getCourtName() {
        return courtName;
    }

    public void setCourtName(String courtName) {
        this.courtName = courtName;
    }

    public int getCourtType() {
        return courtType;
    }

    public void setCourtType(int courtType) {
        this.courtType = courtType;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (courtId != null ? courtId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the courtId fields are not set
        if (!(object instanceof CourtInformation)) {
            return false;
        }
        CourtInformation other = (CourtInformation) object;
        if ((this.courtId == null && other.courtId != null) || (this.courtId != null && !this.courtId.equals(other.courtId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.admin.model.entity.Court[ id=" + courtId + " ]";
    }
    
}
