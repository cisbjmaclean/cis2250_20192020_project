package info.hccis.admin.model.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *  Entity For courtSearch Parameters
 * @author Brandon
 * @since Dec 11 2019
 */
public class CourtSearch {
    //list of varaibles
    @Basic(optional = false)
    @NotNull
    @Column(name = "courtType")
    private int courtType;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "bookingDate")
    private String bookingDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "startTime")
    private String startTime;
    /**
     * Default Constructor
     *
     * @author Brandon
     * @since Dec 11 2019
     */
    public CourtSearch(){
        
    }
    /**
     * Constructor for courtType, bookingDate, startTime
     *
     * @author Brandon
     * @since Dec 11 2019
     */
    public CourtSearch(int courtType,String bookingDate,String startTime){
        this.courtType = courtType;
        this.bookingDate = bookingDate;
        this.startTime = startTime;
    }
    //list of getters and setters
    public int getCourtType() {
        return courtType;
    }

    public void setCourtType(int courtType) {
        this.courtType = courtType;
    }
    
    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
}
