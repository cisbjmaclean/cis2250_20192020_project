package info.hccis.admin.model.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity for found court which match the parameters
 * @author Brandon
 * @since Dec 11 2019
 */
public class CourtSearchFound {
    //List of variables
    @Basic(optional = false)
    @NotNull
    @Column(name = "courtNumber")
    private int courtNumber;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "courtName")
    private String courtName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "courtType")
    private int courtType;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "bookingDate")
    private String bookingDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "startTime")
    private String startTime;
    /**
     * Default Constructor
     *
     * @author Brandon
     * @since Dec 11 2019
     */
     public CourtSearchFound(){
        
    }
     /**
     * Constructor with courtNumber,courtName,courtType,bookingDate,startTime
     *
     * @author Brandon
     * @since Dec 11 2019
     */
    public CourtSearchFound(int courtNumber,String courtName,int courtType,String bookingDate,String startTime){
        this.courtNumber = courtNumber;
        this.courtName = courtName;
        this.courtType = courtType;
        this.bookingDate = bookingDate;
        this.startTime = startTime;
    }
    //List of getters and setters
    public int getCourtNumber() {
        return courtNumber;
    }

    public void setCourtNumber(int courtNumber) {
        this.courtNumber = courtNumber;
    }

    public String getCourtName() {
        return courtName;
    }

    public void setCourtName(String courtName) {
        this.courtName = courtName;
    }
    
    public int getCourtType() {
        return courtType;
    }

    public void setCourtType(int courtType) {
        this.courtType = courtType;
    }
    
    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
}
